.PHONY: build install 

all: install

build:
	@go build ./...

install:
	@go install ./cmd/spectrum.go
