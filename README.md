Spectrum
========

Since cosmos upgraded to stargate `v0.40`, the logs are very verbose and hard
to see your application's logs.

This binary is designed to pipe the cosmos app logs (stdin) into the binary
and then it will cleanup and print the logs in a more readable way
