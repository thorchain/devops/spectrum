package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/thorchain/devops/spectrum"
)

var tendermintModules = []string{"consensus", "pex"}

func main() {
	hiddenModules := flag.String("hide-modules", "", "Comma separated list of modules to hide")
	hideTendermint := flag.Bool("hide-tendermint", true, "Hide tendermint logs")
	flag.Parse()

	mods := make([]string, len(strings.Split(*hiddenModules, ",")))
	for i, m := range strings.Split(*hiddenModules, ",") {
		mods[i] = strings.TrimSpace(m)
	}

	if *hideTendermint {
		mods = append(mods, tendermintModules...)
	}

	spec := Spectrum{
		HiddenModules: mods,
	}

	spec.Start()
}

func isJSON(s string) bool {
	var js map[string]interface{}
	return json.Unmarshal([]byte(s), &js) == nil

}

type Spectrum struct {
	HiddenModules []string
}

func (s Spectrum) Start() error {
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		text := scanner.Text()
		for _, line := range strings.Split(text, "\n") {
			if err := s.processLine(line); err != nil {
				fmt.Printf("failed to process line: %s\n%s\n", line, err)
			}
		}
	}

	if err := scanner.Err(); err != nil {
		log.Printf("Failed scanning line: %s\n%s\n", scanner.Text(), err)
	}
	return nil
}

func (s Spectrum) processLine(raw string) error {
	// test if raw is json format
	var js map[string]interface{}
	isJSON := json.Unmarshal([]byte(raw), &js) == nil

	fmt.Printf(">>>>> %s\n", raw)
	if isJSON {
		return s.processJSON(raw)
	} else {
		return s.processText(raw)
	}
	return nil
}

func (s Spectrum) processJSON(raw string) error {
	var line spectrum.LogLine
	err := json.Unmarshal([]byte(raw), &line)
	if err != nil {
		return fmt.Errorf("failed to parse log line: %s", err)
	}

	err = json.Unmarshal([]byte(raw), &line.Misc)
	if err != nil {
		return fmt.Errorf("failed to parse misc:", err)
	}

	// cleanup misc
	for _, key := range []string{"module", "time", "level", "message", "error"} {
		delete(line.Misc, key)
	}

	if !s.ShouldHideModule(line.Module) {
		line.Print()
	}
	return nil
}

func (s Spectrum) processText(raw string) error {
	fmt.Println(raw)
	return nil
}

func (s Spectrum) ShouldHideModule(mod string) bool {
	for _, m := range s.HiddenModules {
		if strings.EqualFold(m, mod) {
			return true
		}
	}
	return false
}
